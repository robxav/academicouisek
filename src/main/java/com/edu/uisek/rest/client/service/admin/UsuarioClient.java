package com.edu.uisek.rest.client.service.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.edu.uisek.dto.Usuario;
import com.edu.uisek.rest.generic.AbstractClient;
import com.edu.uisek.rest.generic.ApplicationEndpoint;
import com.edu.uisek.rest.generic.ServiceException;
import com.google.gson.Gson;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class UsuarioClient extends AbstractClient {
	private static final Logger log = Logger.getLogger(UsuarioClient.class.getName());

	
	public UsuarioClient(String url, String contextPath) {
		super(url, contextPath);

	}

	public Usuario login(String usuario, String password) {

		WebTarget client = createClient(ApplicationEndpoint.login(usuario, password));
		Response response = client.request().get();
		String resultado = response.readEntity(String.class);
		Usuario obj = new Gson().fromJson(resultado, Usuario.class);
		return obj;

	}

	
}
