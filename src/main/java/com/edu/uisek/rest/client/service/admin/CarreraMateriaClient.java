package com.edu.uisek.rest.client.service.admin;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.edu.uisek.dto.CarreraMateria;
import com.edu.uisek.dto.DetalleMatricula;
import com.edu.uisek.rest.generic.AbstractClient;
import com.edu.uisek.rest.generic.ApplicationEndpoint;
import com.edu.uisek.rest.generic.ServiceException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class CarreraMateriaClient extends AbstractClient {

	public CarreraMateriaClient(String url, String contextPath) {
		super(url, contextPath);

	}

	public List<CarreraMateria> listar(Integer idCarrera, Integer idPensum)
			throws ServiceException {
		List<CarreraMateria> listaAgenciaes = new ArrayList<>();
		WebTarget client = createClient(ApplicationEndpoint.findCarreraMateria(idCarrera, idPensum));
		Response response = client.request().get();
		Integer status = response.getStatus();
		if (Status.OK.getStatusCode() == status) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String resultado = response.readEntity(String.class);

			listaAgenciaes = new Gson().fromJson(resultado, new TypeToken<ArrayList<CarreraMateria>>() {
			}.getType());
		} else {
			throw new ServiceException(response.readEntity(String.class), status);
		}
		return listaAgenciaes;
	}
	
	public CarreraMateria actualizar(Integer id, CarreraMateria Agencia) {

		WebTarget client = createClient(ApplicationEndpoint.actualizarInteger(id));

		Invocation.Builder invocationBuilder = client.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.put(Entity.entity(Agencia, MediaType.APPLICATION_JSON));

		String resultado = response.readEntity(String.class);

		CarreraMateria obj = new Gson().fromJson(resultado,  CarreraMateria.class);
		
		return obj;
	}

}
