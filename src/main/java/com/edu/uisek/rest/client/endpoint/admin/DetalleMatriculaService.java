package com.edu.uisek.rest.client.endpoint.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.edu.uisek.dto.DetalleMatricula;
import com.edu.uisek.rest.client.service.admin.DetalleMatriculaClient;
import com.edu.uisek.rest.generic.ServiceException;

@Stateless
public class DetalleMatriculaService implements Serializable {

	private static final long serialVersionUID = 1L;
	final String URL = "http://localhost:4008";
	// final String URL = "https://servicioagencias.herokuapp.com/";
	final String PATH = "/servicio-detallematri";

	private DetalleMatriculaClient client = new DetalleMatriculaClient(URL, PATH);

	public List<DetalleMatricula> listar(Integer idCarrera, Integer idPensum, Integer idEstudiante) {
		List<DetalleMatricula> list = new ArrayList<>();
		try {
			list.addAll(client.listar(idCarrera, idPensum, idEstudiante));
		} catch (ServiceException ex) {
			ex.toString();
		}
		return list;
	}

	public void insertar(DetalleMatricula entidad) {
		client.crear(entidad);
	}

	public void actualizar(Integer id, DetalleMatricula entidad) {
		DetalleMatricula obj;
		obj = client.actualizar(id, entidad);
	}

	public void eliminar(Integer id) {
		client.eliminar(id);
	}

}
