package com.edu.uisek.rest.client.endpoint.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.edu.uisek.dto.CarreraMateria;
import com.edu.uisek.dto.DetalleMatricula;
import com.edu.uisek.rest.client.service.admin.CarreraMateriaClient;
import com.edu.uisek.rest.client.service.admin.DetalleMatriculaClient;
import com.edu.uisek.rest.generic.ServiceException;

@Stateless
public class CarreraMateriaService implements Serializable {

	private static final long serialVersionUID = 1L;
	final String URL = "http://localhost:4008";
	// final String URL = "https://servicioagencias.herokuapp.com/";
	final String PATH = "/servicio-carmat";

	private CarreraMateriaClient client = new CarreraMateriaClient(URL, PATH);

	public List<CarreraMateria> listar(Integer idCarrera, Integer idPensum) {
		List<CarreraMateria> list = new ArrayList<>();
		try {
			list.addAll(client.listar(idCarrera, idPensum));
		} catch (ServiceException ex) {
			ex.toString();
		}
		return list;
	}

	public void actualizar(Integer id, CarreraMateria entidad) {
		CarreraMateria obj;
		obj = client.actualizar(id, entidad);
	}

}
