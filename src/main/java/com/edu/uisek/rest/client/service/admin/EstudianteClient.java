package com.edu.uisek.rest.client.service.admin;

import java.util.logging.Logger;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.edu.uisek.dto.Estudiante;
import com.edu.uisek.dto.Usuario;
import com.edu.uisek.rest.generic.AbstractClient;
import com.edu.uisek.rest.generic.ApplicationEndpoint;
import com.google.gson.Gson;

public class EstudianteClient extends AbstractClient{
	
private static final Logger log = Logger.getLogger(UsuarioClient.class.getName());

	
	public EstudianteClient(String url, String contextPath) {
		super(url, contextPath);

	}
	
	public Estudiante findByCedula(String cedula) {

		WebTarget client = createClient(ApplicationEndpoint.findbycedula(cedula));
		Response response = client.request().get();
		String resultado = response.readEntity(String.class);
		Estudiante obj = new Gson().fromJson(resultado, Estudiante.class);
		return obj;

	}

}
