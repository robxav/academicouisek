package com.edu.uisek.rest.client.service.admin;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.edu.uisek.dto.Carrera;
import com.edu.uisek.dto.Periodo;
import com.edu.uisek.rest.generic.AbstractClient;
import com.edu.uisek.rest.generic.ApplicationEndpoint;
import com.google.gson.Gson;

public class CarreraClient extends AbstractClient {

	public CarreraClient(String url, String contextPath) {
		super(url, contextPath);

	}

	public Carrera findCarrerabyID(Integer idCarrera) {

		WebTarget client = createClient(ApplicationEndpoint.buscarPorIdInteger(idCarrera));
		Response response = client.request().get();
		String resultado = response.readEntity(String.class);
		Carrera obj = new Gson().fromJson(resultado, Carrera.class);
		return obj;

	}

}
