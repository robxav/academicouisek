package com.edu.uisek.rest.client.service.admin;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.edu.uisek.dto.Periodo;
import com.edu.uisek.dto.Usuario;
import com.edu.uisek.rest.generic.AbstractClient;
import com.edu.uisek.rest.generic.ApplicationEndpoint;
import com.google.gson.Gson;

public class PeriodoClient extends AbstractClient {

	public PeriodoClient(String url, String contextPath) {
		super(url, contextPath);

	}

	public Periodo findPeriodoVigente(Boolean vigente) {

		WebTarget client = createClient(ApplicationEndpoint.findByVigencia(vigente));
		Response response = client.request().get();
		String resultado = response.readEntity(String.class);
		Periodo obj = new Gson().fromJson(resultado, Periodo.class);
		return obj;

	}

}
