package com.edu.uisek.rest.client.service.admin;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.edu.uisek.dto.Carrera;
import com.edu.uisek.dto.Inscripcion;
import com.edu.uisek.rest.generic.AbstractClient;
import com.edu.uisek.rest.generic.ApplicationEndpoint;
import com.google.gson.Gson;

public class InscripcionClient extends AbstractClient {

	public InscripcionClient(String url, String contextPath) {
		super(url, contextPath);

	}

	public Inscripcion findInsByEstudianteCarrea(Integer idEstudiante, Integer idPensum) {

		WebTarget client = createClient(ApplicationEndpoint.findInscripcionByEstudiante(idEstudiante, idPensum));
		Response response = client.request().get();
		String resultado = response.readEntity(String.class);
		Inscripcion obj = new Gson().fromJson(resultado, Inscripcion.class);
		return obj;

	}

}
