package com.edu.uisek.rest.client.endpoint.admin;

import java.io.Serializable;

import com.edu.uisek.dto.Carrera;
import com.edu.uisek.dto.Inscripcion;
import com.edu.uisek.rest.client.service.admin.CarreraClient;
import com.edu.uisek.rest.client.service.admin.InscripcionClient;

public class InscripcionService implements Serializable {

	private static final long serialVersionUID = 1L;
	final String URL = "http://localhost:4008";
	// final String URL = "https://servicioagencias.herokuapp.com/";
	final String PATH = "/servicio-inscripcion";

	private InscripcionClient client = new InscripcionClient(URL, PATH);

	public Inscripcion findByEstudiantePensum(Integer idEstudiante, Integer idPeriodo) {

		Inscripcion per = new Inscripcion();

		try {
			per = client.findInsByEstudianteCarrea(idEstudiante, idPeriodo);

			if (per != null) {
				return per;
			} else {
				return null;
			}

		} catch (Exception e) {
			return null;
		}

	}

}
