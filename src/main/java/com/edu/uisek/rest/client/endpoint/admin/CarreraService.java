package com.edu.uisek.rest.client.endpoint.admin;

import java.io.Serializable;

import javax.ejb.Stateless;

import com.edu.uisek.dto.Carrera;
import com.edu.uisek.dto.Periodo;
import com.edu.uisek.rest.client.service.admin.CarreraClient;
import com.edu.uisek.rest.client.service.admin.PeriodoClient;

@Stateless
public class CarreraService implements Serializable {

	private static final long serialVersionUID = 1L;
	final String URL = "http://localhost:4008";
	// final String URL = "https://servicioagencias.herokuapp.com/";
	final String PATH = "/servicio-carrera";

	private CarreraClient client = new CarreraClient(URL, PATH);

	public Carrera findCarrera(Integer idCarrera) {

		Carrera per = new Carrera();

		try {
			per = client.findCarrerabyID(idCarrera);

			if (per != null) {
				return per;
			} else {
				return null;
			}

		} catch (Exception e) {
			return null;
		}

	}

}
