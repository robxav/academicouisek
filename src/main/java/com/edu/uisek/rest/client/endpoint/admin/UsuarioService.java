package com.edu.uisek.rest.client.endpoint.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.xml.registry.infomodel.User;
import com.edu.uisek.dto.Usuario;
import com.edu.uisek.rest.client.service.admin.UsuarioClient;
import com.edu.uisek.rest.generic.ServiceException;

@Stateless
public class UsuarioService implements Serializable {

	private static final long serialVersionUID = 1L;
	final String URL = "http://localhost:4008";
	// final String URL = "https://servicioagencias.herokuapp.com/";
	final String PATH = "/servicio-usuario";

	private UsuarioClient client = new UsuarioClient(URL, PATH);

	public Usuario login(String usuario, String passwd) {
		Usuario user = new Usuario();
		try {
			user = client.login(usuario, passwd);

			if (user != null) {
				return user;
			} else {
				return null;
			}

		} catch (Exception e) {
			return null;
		}

	}

}
