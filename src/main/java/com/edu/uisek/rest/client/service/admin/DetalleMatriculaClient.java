package com.edu.uisek.rest.client.service.admin;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.edu.uisek.dto.DetalleMatricula;
import com.edu.uisek.rest.generic.AbstractClient;
import com.edu.uisek.rest.generic.ApplicationEndpoint;
import com.edu.uisek.rest.generic.ServiceException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class DetalleMatriculaClient extends AbstractClient {

	public DetalleMatriculaClient(String url, String contextPath) {
		super(url, contextPath);

	}

	public List<DetalleMatricula> listar(Integer idCarrera, Integer idPensum, Integer idEstudiante)
			throws ServiceException {
		List<DetalleMatricula> listaAgenciaes = new ArrayList<>();
		WebTarget client = createClient(ApplicationEndpoint.findDetalleMatricula(idCarrera, idPensum, idEstudiante));
		Response response = client.request().get();
		Integer status = response.getStatus();
		if (Status.OK.getStatusCode() == status) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String resultado = response.readEntity(String.class);

			listaAgenciaes = new Gson().fromJson(resultado, new TypeToken<ArrayList<DetalleMatricula>>() {
			}.getType());
		} else {
			throw new ServiceException(response.readEntity(String.class), status);
		}
		return listaAgenciaes;
	}
	
	public DetalleMatricula actualizar(Integer id, DetalleMatricula Agencia) {

		WebTarget client = createClient(ApplicationEndpoint.actualizarInteger(id));

		Invocation.Builder invocationBuilder = client.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.put(Entity.entity(Agencia, MediaType.APPLICATION_JSON));

		String resultado = response.readEntity(String.class);

		DetalleMatricula obj = new Gson().fromJson(resultado,  DetalleMatricula.class);
		
		return obj;
	}

	public void eliminar(Integer id) {

		WebTarget client = createClient(ApplicationEndpoint.eliminarInteger(id));
		Invocation.Builder invocationBuilder = client.request();
		invocationBuilder.delete();

	}

	public DetalleMatricula crear(DetalleMatricula Agencia) {

		WebTarget client = createClient(ApplicationEndpoint.insertar());

		Invocation.Builder invocationBuilder = client.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(Agencia, MediaType.APPLICATION_JSON));

		String resultado = response.readEntity(String.class);

		DetalleMatricula obj = new Gson().fromJson(resultado,  DetalleMatricula.class);
		
		return obj;
	}

}
