package com.edu.uisek.rest.client.endpoint.admin;

import java.io.Serializable;

import javax.ejb.Stateless;

import com.edu.uisek.dto.Estudiante;
import com.edu.uisek.dto.Usuario;
import com.edu.uisek.rest.client.service.admin.EstudianteClient;

@Stateless
public class EstudianteService implements Serializable {

	private static final long serialVersionUID = 1L;
	final String URL = "http://localhost:4008";
	// final String URL = "https://servicioagencias.herokuapp.com/";
	final String PATH = "/servicio-estudiante";

	private EstudianteClient estudiante = new EstudianteClient(URL, PATH);

	public Estudiante findByCedula(String cedula) {
		Estudiante estud = new Estudiante();
		try {
			estud = estudiante.findByCedula(cedula);

			if (estud != null) {
				return estud;
			} else {
				return null;
			}

		} catch (Exception e) {
			return null;
		}

	}

}
