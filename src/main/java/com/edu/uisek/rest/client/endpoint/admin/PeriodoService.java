package com.edu.uisek.rest.client.endpoint.admin;

import java.io.Serializable;

import javax.ejb.Stateless;

import com.edu.uisek.dto.Periodo;
import com.edu.uisek.dto.Usuario;
import com.edu.uisek.rest.client.service.admin.PeriodoClient;
import com.edu.uisek.rest.client.service.admin.UsuarioClient;

@Stateless
public class PeriodoService implements Serializable {

	private static final long serialVersionUID = 1L;
	final String URL = "http://localhost:4008";
	// final String URL = "https://servicioagencias.herokuapp.com/";
	final String PATH = "/servicio-periodo";

	private PeriodoClient client = new PeriodoClient(URL, PATH);

	public Periodo findVigente(Boolean vigente) {

		Periodo per = new Periodo();

		try {
			per = client.findPeriodoVigente(vigente);

			if (per != null) {
				return per;
			} else {
				return null;
			}

		} catch (Exception e) {
			return null;
		}

	}

}
