/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edu.uisek.rest.generic;

/**
 *
 * @author Usuario
 */
public class ApplicationEndpoint {

	public static String listar() {
		return "/listar";
	}

	public static String insertar() {

		return "/crear";
	}

	public static String actualizar(Long id) {
		return "/editar/" + id;
	}

	public static String actualizarPK(Long idTabla1, Long idTabla2) {
		return "/editar/" + idTabla1 + "/" + idTabla2;
	}

	public static String buscarPorId(Long id) {
		return "/ver/" + id;
	}

	public static String buscarPorIdInteger(Integer id) {
		return "/ver/" + id;
	}

	public static String findbycedula(String cedula) {
		return "/find/" + cedula;
	}

	public static String findByVigencia(Boolean vigente) {
		return "/find/" + vigente;
	}

	public static String eliminar(Long id) {
		return "/eliminar/" + id;
	}
	
	
	public static String eliminarInteger(Integer id) {
		return "/eliminar/" + id;
	}

	public static String login(String usuario, String pass) {
		return "/login/" + usuario + "/" + pass;
	}

	public static String loginML(String access_token) {
		return "/ML_USER/" + access_token;
	}

	public static String itemsML(String acces_token, String id_user) {
		return "/ML_ITEMS/" + acces_token + "/" + id_user;
	}

	public static String findInscripcionByEstudiante(Integer idEstudiante, Integer Idperiodo) {
		return "/find/" + idEstudiante + "/" + Idperiodo;
	}

	public static String findDetalleMatricula(Integer idCarrera, Integer Idperiodo, Integer idEstudiante) {
		return "/listar/" + idCarrera + "/" + Idperiodo + "/" + idEstudiante;
	}
	
	public static String findCarreraMateria(Integer idCarrera, Integer Idperiodo) {
		return "/listar/" + idCarrera + "/" + Idperiodo ;
	}
	
	public static String actualizarInteger(Integer id) {
		return "/editar/" + id;
	}

}
