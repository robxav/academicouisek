package com.edu.uisek.recursos;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.omnifaces.util.Faces;

@ManagedBean(name = "sesionController")
@SessionScoped
public class SesionController implements Serializable {

	private static final long serialVersionUID = -468877955954871485L;
	private FacesContext fc = FacesContext.getCurrentInstance();
	private HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
	private HttpSession session = request.getSession();
	// private Usuario currentUser = (Usuario) session.getAttribute("currentUser");

	public void timeout() throws IOException {
		Utilitarios util = new Utilitarios();
		String urlAplicacion = fc.getExternalContext().getInitParameter("urlAplicacion");
		util.mostrarMensaje("SESION CADUCADA", "Error en Logeo, Verificar Datos", "E");

		session.invalidate();
		Faces.getExternalContext().getFlash().setKeepMessages(true);
		Faces.redirect(urlAplicacion);
	}

	/*
	 * public Usuario getCurrentUser() { return currentUser; }
	 * 
	 * public void setCurrentUser(Usuario currentUser) { this.currentUser =
	 * currentUser; }
	 */

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

}
