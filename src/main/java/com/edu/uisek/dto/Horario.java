package com.edu.uisek.dto;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.List;


/**
 * The persistent class for the horarios database table.
 * 
 */

public class Horario implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private Integer idHorario;

	
	private Integer idPensum;

	private String jueves;

	private String lunes;

	private String martes;

	private String miercoles;

	private String sabado;

	private String viernes;

	private Boolean vigente;

	
	private Materia materia;

	public Horario() {
	}

	public Integer getIdHorario() {
		return this.idHorario;
	}

	public void setIdHorario(Integer idHorario) {
		this.idHorario = idHorario;
	}

	public Integer getIdPensum() {
		return this.idPensum;
	}

	public void setIdPensum(Integer idPensum) {
		this.idPensum = idPensum;
	}

	public String getJueves() {
		return this.jueves;
	}

	public void setJueves(String jueves) {
		this.jueves = jueves;
	}

	public String getLunes() {
		return this.lunes;
	}

	public void setLunes(String lunes) {
		this.lunes = lunes;
	}

	public String getMartes() {
		return this.martes;
	}

	public void setMartes(String martes) {
		this.martes = martes;
	}

	public String getMiercoles() {
		return this.miercoles;
	}

	public void setMiercoles(String miercoles) {
		this.miercoles = miercoles;
	}

	public String getSabado() {
		return this.sabado;
	}

	public void setSabado(String sabado) {
		this.sabado = sabado;
	}

	public String getViernes() {
		return this.viernes;
	}

	public void setViernes(String viernes) {
		this.viernes = viernes;
	}

	public Boolean getVigente() {
		return this.vigente;
	}

	public void setVigente(Boolean vigente) {
		this.vigente = vigente;
	}

	

	

	public Materia getMateria() {
		return this.materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

}