package com.edu.uisek.dto;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.Date;


/**
 * The persistent class for the detalle_matricula database table.
 * 
 */

public class DetalleMatricula implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private Integer idDetalematricula;

	
	private Date fecha;

	private Boolean legalizado;

	
	private CarreraMateria carreraMateria;

	
	private Inscripcion inscripcion;

	public DetalleMatricula() {
	}

	public Integer getIdDetalematricula() {
		return this.idDetalematricula;
	}

	public void setIdDetalematricula(Integer idDetalematricula) {
		this.idDetalematricula = idDetalematricula;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Boolean getLegalizado() {
		return this.legalizado;
	}

	public void setLegalizado(Boolean legalizado) {
		this.legalizado = legalizado;
	}

	public CarreraMateria getCarreraMateria() {
		return this.carreraMateria;
	}

	public void setCarreraMateria(CarreraMateria carreraMateria) {
		this.carreraMateria = carreraMateria;
	}

	public Inscripcion getInscripcion() {
		return this.inscripcion;
	}

	public void setInscripcion(Inscripcion inscripcion) {
		this.inscripcion = inscripcion;
	}

}