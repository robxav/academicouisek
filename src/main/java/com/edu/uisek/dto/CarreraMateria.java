package com.edu.uisek.dto;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.List;


/**
 * The persistent class for the carrera_materias database table.
 * 
 */

public class CarreraMateria implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private Integer idCarreraMnateria;

	private String aula;

	
	private Integer cupoDisponible;

	
	private Integer cupoOcupado;

	
	private Integer cupoTotal;

	private String grupo;

	
	private Carrera carrera;

	
	private Horario horario;

	

	public CarreraMateria() {
	}

	public Integer getIdCarreraMnateria() {
		return this.idCarreraMnateria;
	}

	public void setIdCarreraMnateria(Integer idCarreraMnateria) {
		this.idCarreraMnateria = idCarreraMnateria;
	}

	public String getAula() {
		return this.aula;
	}

	public void setAula(String aula) {
		this.aula = aula;
	}

	public Integer getCupoDisponible() {
		return this.cupoDisponible;
	}

	public void setCupoDisponible(Integer cupoDisponible) {
		this.cupoDisponible = cupoDisponible;
	}

	public Integer getCupoOcupado() {
		return this.cupoOcupado;
	}

	public void setCupoOcupado(Integer cupoOcupado) {
		this.cupoOcupado = cupoOcupado;
	}

	public Integer getCupoTotal() {
		return this.cupoTotal;
	}

	public void setCupoTotal(Integer cupoTotal) {
		this.cupoTotal = cupoTotal;
	}

	public String getGrupo() {
		return this.grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Carrera getCarrera() {
		return this.carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public Horario getHorario() {
		return this.horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}

	

}