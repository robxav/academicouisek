package com.edu.uisek.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.PrimeFaces;

import com.edu.uisek.dto.Carrera;
import com.edu.uisek.dto.CarreraMateria;
import com.edu.uisek.dto.DetalleMatricula;
import com.edu.uisek.dto.Estudiante;
import com.edu.uisek.dto.Inscripcion;
import com.edu.uisek.dto.Periodo;
import com.edu.uisek.dto.Usuario;
import com.edu.uisek.recursos.SesionController;
import com.edu.uisek.recursos.Utilitarios;
import com.edu.uisek.rest.client.endpoint.admin.CarreraMateriaService;
import com.edu.uisek.rest.client.endpoint.admin.CarreraService;
import com.edu.uisek.rest.client.endpoint.admin.DetalleMatriculaService;
import com.edu.uisek.rest.client.endpoint.admin.EstudianteService;
import com.edu.uisek.rest.client.endpoint.admin.InscripcionService;
import com.edu.uisek.rest.client.endpoint.admin.PeriodoService;
import com.edu.uisek.rest.client.endpoint.admin.UsuarioService;
import com.github.adminfaces.template.config.AdminConfig;


@ManagedBean(name = "inscripcionesController")
@SessionScoped
public class InscripcionesController extends SesionController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	FacesContext fc = FacesContext.getCurrentInstance();
	HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
	HttpSession session = request.getSession();
	Usuario su = (Usuario) session.getAttribute("currentUser");

	private Estudiante estudiante;
	private Periodo periodo;
	private Inscripcion inscripcion;
	private Carrera carrera;
	private DetalleMatricula detalle;
	private List<DetalleMatricula> listadetalles = new ArrayList<DetalleMatricula>();
	private CarreraMateria ofertaSelect;
	private List<CarreraMateria> ofertas = new ArrayList<CarreraMateria>();
	private Utilitarios util;

	@Inject
	private AdminConfig adminConfig;
	@Inject
	private EstudianteService estudianteSB;
	@Inject
	private PeriodoService periodoSB;
	@Inject
	private CarreraService carreraSB;
	@Inject
	private InscripcionService inscripcionSB;
	@Inject
	private DetalleMatriculaService detalleSB;
	@Inject
	private CarreraMateriaService carMatSB;

	@PostConstruct
	public void init() {
		try {

			setEstudiante(estudianteSB.findByCedula(su.getCedula().trim()));
			periodo = periodoSB.findVigente(true);
			inscripcion = inscripcionSB.findByEstudiantePensum(estudiante.getIdEstudiante(), periodo.getIdPensum());
			carrera = carreraSB.findCarrera(inscripcion.getIdCarrera());
			listadetalles = detalleSB.listar(carrera.getIdCarrera(), periodo.getIdPensum(),
					estudiante.getIdEstudiante());
			ofertaSelect = new CarreraMateria();
			detalle = new DetalleMatricula();
			ofertas = carMatSB.listar(carrera.getIdCarrera(), periodo.getIdPensum());
			util = new Utilitarios();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void incribirMateria() {
		try {
			System.out.println("selest" + ofertaSelect.getIdCarreraMnateria());

			DetalleMatricula deta = new DetalleMatricula();

			deta.setCarreraMateria(ofertaSelect);
			deta.setFecha(new Date());
			deta.setInscripcion(inscripcion);
			deta.setLegalizado(false);
			detalleSB.insertar(deta);
			
			init();
			
			util.mostrarMensaje("INFO.",
					"Datos almacenados exitosamente!.", "I");

			PrimeFaces.current().executeScript("PF('dlg1').hide();");
		} catch (Exception e) {
			util.mostrarMensaje("ERROR.",
					"Se ha producido un error al inscribirse en la materia!.", "W");
		}

	}

	public Estudiante getEstudiante() {
		return estudiante;
	}

	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public Inscripcion getInscripcion() {
		return inscripcion;
	}

	public void setInscripcion(Inscripcion inscripcion) {
		this.inscripcion = inscripcion;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public DetalleMatricula getDetalle() {
		return detalle;
	}

	public void setDetalle(DetalleMatricula detalle) {
		this.detalle = detalle;
	}

	public List<DetalleMatricula> getListadetalles() {
		return listadetalles;
	}

	public void setListadetalles(List<DetalleMatricula> listadetalles) {
		this.listadetalles = listadetalles;
	}

	public CarreraMateria getOfertaSelect() {
		return ofertaSelect;
	}

	public void setOfertaSelect(CarreraMateria ofertaSelect) {
		this.ofertaSelect = ofertaSelect;
	}

	public List<CarreraMateria> getOfertas() {
		return ofertas;
	}

	public void setOfertas(List<CarreraMateria> ofertas) {
		this.ofertas = ofertas;
	}

	public Utilitarios getUtil() {
		return util;
	}

	public void setUtil(Utilitarios util) {
		this.util = util;
	}
	

}
