package com.edu.uisek.security;

import static com.edu.uisek.util.Utils.addDetailMessage;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Init;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Specializes;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.omnifaces.util.Faces;

import com.edu.uisek.dto.Usuario;
import com.edu.uisek.recursos.Utilitarios;
import com.edu.uisek.rest.client.endpoint.admin.UsuarioService;
import com.github.adminfaces.template.config.AdminConfig;
import com.github.adminfaces.template.session.AdminSession;

import io.swagger.client.api.DefaultApiExample;

/**
 * Created by rmpestano on 12/20/14.
 *
 * This is just a login example.
 *
 * AdminSession uses isLoggedIn to determine if user must be redirect to login
 * page or not. By default AdminSession isLoggedIn always resolves to true so it
 * will not try to redirect user.
 *
 * If you already have your authorization mechanism which controls when user
 * must be redirect to initial page or logon you can skip this class.
 */
@Named
@SessionScoped
@Specializes
public class LogonMB extends AdminSession implements Serializable {

	private Utilitarios util = new Utilitarios();

	private String currentUser;
	private String email;
	private String password;
	private boolean remember;
	private Usuario userLoggin;

	@Inject
	private AdminConfig adminConfig;

	@Inject
	private UsuarioService service;

	public void login() throws IOException {
		currentUser = email;
		String urlAplicacion = Faces.getExternalContext().getInitParameter("urlAplicacion");

		System.out.println("Aplicacion" + urlAplicacion);
		userLoggin = service.login(email, password);

		if (userLoggin != null)

		{
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
			HttpSession session = request.getSession();
			session.setAttribute("currentUser", userLoggin);

			addDetailMessage("Ingreso exitoso: <b>" + email + "</b>");
			Faces.getExternalContext().getFlash().setKeepMessages(true);
			Faces.redirect(adminConfig.getIndexPage());

		} else {
			errorSession();
		}

	}

	public void errorSession() {
		try {
			String urlAplicacion = Faces.getExternalContext().getInitParameter("urlAplicacion");
			util.mostrarMensaje("CREDENCIALES INCORRECTAS", "Error en Logeo, Verificar Datos", "E");
			HttpServletRequest request = (HttpServletRequest) Faces.getExternalContext().getRequest();
			HttpSession session = request.getSession();
			session.invalidate();
			Faces.getExternalContext().getFlash().setKeepMessages(true);
			Faces.redirect(urlAplicacion);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isLoggedIn() {
		return currentUser != null;

	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isRemember() {
		return remember;
	}

	public void setRemember(boolean remember) {
		this.remember = remember;
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}

	public Usuario getUserLoggin() {
		return userLoggin;
	}

	public void setUserLoggin(Usuario userLoggin) {
		this.userLoggin = userLoggin;
	}

}
